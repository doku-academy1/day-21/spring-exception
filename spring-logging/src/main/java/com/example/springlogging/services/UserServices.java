package com.example.springlogging.services;

import com.example.springlogging.dtos.CreateUserRequestDTO;
import com.example.springlogging.dtos.CreateUserResponseDTO;
import com.example.springlogging.entity.User;
import com.example.springlogging.exception.DataAlreadyExistException;
import com.example.springlogging.exception.DataDoesntExist;
import com.example.springlogging.exception.ValidationErrorException;
import com.example.springlogging.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServices implements IUserServices {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ModelMapper modelMapper;

    @Override
    @Transactional
    public CreateUserResponseDTO createNewUser(CreateUserRequestDTO requestDTO) {
        validate(requestDTO);
        User user = convertToEntity(requestDTO);

        Optional<User> optionalUser;
        optionalUser = userRepository.findByUsername(requestDTO.getUsername());
        if (optionalUser.isPresent()) {
            throw new DataAlreadyExistException("Data with username " + requestDTO.getUsername() + " already exist.");
        }
        optionalUser = userRepository.findByEmail(requestDTO.getEmail());
        if (optionalUser.isPresent()) {
            throw new DataAlreadyExistException("Data with email " + requestDTO.getEmail() + " already exist.");
        }

        User createdUser = userRepository.save(user);
        log.info("User sudah terbuat");
        return convertToDTO(createdUser);
    }

    public List<CreateUserResponseDTO> getAllUsers() {
        List<User> users = (List<User>) userRepository.findAll();
        if (!users.isEmpty()) {
            return users.stream().map(user -> modelMapper.map(user, CreateUserResponseDTO.class)).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public CreateUserResponseDTO getUserById(Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if(optionalUser.isEmpty()) {
            throw new DataAlreadyExistException("Data with email is not  exist.");
        }
        User user = optionalUser.get();
        return convertToDTO(user);
    }

    public CreateUserResponseDTO updateUserById (CreateUserRequestDTO users, Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        User updateUser = optionalUser.map(user -> {
            user.setUsername(users.getUsername());
            user.setEmail(users.getEmail());
            user.setPassword(users.getPassword());
            return userRepository.save(user);
        }).orElseGet(() -> {return userRepository.save(convertToEntity(users));});
        return convertToDTO(updateUser);
    }

    public void deleteUserById (Long userId) {
        userRepository.deleteById(userId);
    }

    private User convertToEntity(CreateUserRequestDTO requestDTO) {
        return modelMapper.map(requestDTO, User.class);
    }

    private CreateUserResponseDTO convertToDTO(User user) {
        return modelMapper.map(user, CreateUserResponseDTO.class);
    }

    private void validate(CreateUserRequestDTO requestDTO) {
        if (requestDTO == null) {
            throw new ValidationErrorException("Body request cannot be empty.");
        }
        else if (requestDTO.getUsername() == null) {
            throw new DataDoesntExist("Username doesn't exist");
        }
    }
}
