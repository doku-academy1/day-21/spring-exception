package com.example.springlogging.services;

import com.example.springlogging.dtos.CreateUserRequestDTO;
import com.example.springlogging.dtos.CreateUserResponseDTO;

import java.util.List;

public interface IUserServices {
    CreateUserResponseDTO createNewUser(CreateUserRequestDTO requestDTO);
    List<CreateUserResponseDTO> getAllUsers();
}
