package com.example.springlogging.exception;

public class ValidationErrorException extends RuntimeException {
    public ValidationErrorException() {
        super();
    }
    public ValidationErrorException(String message) {
        super(message);
    }
}