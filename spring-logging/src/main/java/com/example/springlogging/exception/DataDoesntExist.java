package com.example.springlogging.exception;

public class DataDoesntExist extends RuntimeException {
    public DataDoesntExist(String message) {
        super(message);
    }
}
