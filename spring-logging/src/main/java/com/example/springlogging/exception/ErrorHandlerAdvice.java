package com.example.springlogging.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorHandlerAdvice {

    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<Object> handleGeneralError(RuntimeException e) {
        return new ResponseEntity<>(
                buildResponse("7001", "General Error, Internal server error."),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler(value = DataAlreadyExistException.class)
    public ResponseEntity<Object> handleDataAlreadyExistException(RuntimeException e) {
        return new ResponseEntity<>(
                buildResponse("7002", "Bad Request, Data already exist."),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(value = ValidationErrorException.class)
    public ResponseEntity<Object> handleValidationError(RuntimeException e) {
        return new ResponseEntity<>(
                buildResponse("7003", "Data Not Found"),
                HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(value = UpdateErrorException.class)
    public ResponseEntity<Object> handleUpdateError(RuntimeException e) {
        return new ResponseEntity<>(
                buildResponse("7004", "Request cannot be updated"),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(value = DataDoesntExist.class)
    public ResponseEntity<Object> handleDataDoesntExist(RuntimeException e) {
        return new ResponseEntity<>(
                buildResponse("7005", "Data is not exist."),
                HttpStatus.BAD_REQUEST
        );
    }

    private Map<String, String> buildResponse(String code, String message) {
        Map<String, String> response = new HashMap<>();
        response.put("code", code);
        response.put("message", message);
        return response;
    }

}
