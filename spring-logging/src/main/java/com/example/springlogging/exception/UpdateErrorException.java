package com.example.springlogging.exception;

public class UpdateErrorException extends RuntimeException {
    public UpdateErrorException(String message) {
        super(message);
    }
}
